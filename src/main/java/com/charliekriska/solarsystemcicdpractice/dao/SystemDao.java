package com.charliekriska.solarsystemcicdpractice.dao;

import com.charliekriska.solarsystemcicdpractice.model.Planet;
import com.charliekriska.solarsystemcicdpractice.model.System;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SystemDao {

    public List<System> getSystems() {
        System system1 = new System();
        List<Planet> planets1 = new ArrayList<Planet>();
        planets1.add(Planet.builder().name("Mercury").id(1).build());
        planets1.add(Planet.builder().name("Venus").id(2).build());
        planets1.add(Planet.builder().name("Earth").id(3).build());
        planets1.add(Planet.builder().name("Mars").id(4).build());
        planets1.add(Planet.builder().name("Jupiter").id(5).build());
        planets1.add(Planet.builder().name("Saturn").id(6).build());
        planets1.add(Planet.builder().name("Uranus").id(7).build());
        planets1.add(Planet.builder().name("Neptune").id(8).build());
        system1.setPlanets(planets1);

        System system2 = new System();
        List<Planet> planets2 = new ArrayList<Planet>();
        planets2.add(Planet.builder().name("Varkon").id(1).build());
        planets2.add(Planet.builder().name("Haylops").id(2).build());
        planets2.add(Planet.builder().name("Tatooine").id(3).build());
        planets2.add(Planet.builder().name("Mordor").id(4).build());
        planets2.add(Planet.builder().name("Cersei").id(5).build());
        system2.setPlanets(planets2);

        List<System> systems = new ArrayList<>();
        systems.add(system1);
        systems.add(system2);

        return systems;
    }

}
