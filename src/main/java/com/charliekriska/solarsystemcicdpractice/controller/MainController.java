package com.charliekriska.solarsystemcicdpractice.controller;

import com.charliekriska.solarsystemcicdpractice.dao.SystemDao;
import com.charliekriska.solarsystemcicdpractice.model.System;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/v1")
public class MainController {

    private SystemDao systemDao;

    @GetMapping("/systems")
    public List<System> getSystems() {
        return systemDao.getSystems();
    }

}
