package com.charliekriska.solarsystemcicdpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolarSystemCicdPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolarSystemCicdPracticeApplication.class, args);
	}

}
