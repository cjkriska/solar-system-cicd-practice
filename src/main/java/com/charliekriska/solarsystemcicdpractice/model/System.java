package com.charliekriska.solarsystemcicdpractice.model;

import lombok.Data;

import java.util.List;

@Data
public class System {
    private List<Planet> planets;
}
