package com.charliekriska.solarsystemcicdpractice.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Planet {
    private int id;
    private String name;
}
